package com.example.basicintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SecondActivity2 extends AppCompatActivity {

    ImageButton sentbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second2);
    }

    public void Profile(View view){
        Button Profile = (Button)findViewById(R.id.btn2);
        Intent intent = new Intent(SecondActivity2.this,SecondActivity.class);
        startActivity(intent);
    }

    public void Logout(View view){
        Button Logout = (Button)findViewById(R.id.btn2);
        Intent intent = new Intent(SecondActivity2.this,MainActivity.class);
        startActivity(intent);
    }

    public void Facebook(View view){
        sentbtn  = (ImageButton)findViewById(R.id.btnf);
        Intent intent = new Intent(SecondActivity2.this,SecondActivity3.class);
        startActivity(intent);
    }

    public void Spotify(View view){
        sentbtn  = (ImageButton)findViewById(R.id.btns);
        Intent intent = new Intent(SecondActivity2.this,SecondActivity4.class);
        startActivity(intent);
    }

    public void Youtube(View view){
        sentbtn  = (ImageButton)findViewById(R.id.btny);
        Intent intent = new Intent(SecondActivity2.this,SecondActivity5.class);
        startActivity(intent);
    }


}